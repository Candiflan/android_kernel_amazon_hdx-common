# MSM8974
   zreladdr-$(CONFIG_ARCH_MSM8974)	:= 0x00008000
ifeq ($(CONFIG_ARCH_MSM8974_THOR),y)
        dtb-$(CONFIG_ARCH_MSM8974)	+= thor-v1.dtb
        dtb-$(CONFIG_ARCH_MSM8974)	+= thor-v2.dtb
        dtb-$(CONFIG_ARCH_MSM8974)	+= thor-v2-apq.dtb
else
ifeq ($(CONFIG_ARCH_MSM8974_APOLLO),y)
        dtb-$(CONFIG_ARCH_MSM8974)	+= apollo-v1.dtb
        dtb-$(CONFIG_ARCH_MSM8974)	+= apollo-v2.dtb
        dtb-$(CONFIG_ARCH_MSM8974)	+= apollo-apq.dtb
endif
endif